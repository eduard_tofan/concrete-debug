<?php

return [
    'providers' => [
        'fire' => 'Concrete\Package\Debug\Src\Provider\FireServiceProvider',
        'clockwork' => 'Concrete\Package\Debug\Src\Provider\ClockworkServiceProvider',
    ],
];
