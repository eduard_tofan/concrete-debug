<?php

return [
    // whether or not the clockwork extension is enabled.
    'enable_clockwork' => false,

    // whether or not the firePHP extension is enabled.
    'enable_fire' => false,

    // whether or not the doctrine data source is enabled.
    // this can slow down the logging because it would
    // log all concrete queries
    'doctrine_data_sourced_enabled' => true
];
