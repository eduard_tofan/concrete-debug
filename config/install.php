<?php

return [
    'singlepages' => [
        [
            'path' => '/dashboard/debug',
            'name' => tc('debug', 'Debug'),
            'description' => tc('debug', 'Debug package settings.')
        ],
    ]
];
