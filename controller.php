<?php

namespace Concrete\Package\Debug;

use Concrete\Core\Package\Package;
use Concrete\Package\Debug\Src\Package\Start;
use Concrete\Package\Debug\Src\Package\Install;

require __DIR__ . '/vendor/autoload.php';

class Controller extends Package
{
    /**
     * The package name/handle.
     *
     * @var string
     */
    protected $pkgHandle = 'debug';

    /**
     * The concrete required version.
     *
     * @var string
     */
    protected $appVersionRequired = '5.7.5.6';

    /**
     * The current package version.
     *
     * @var string
     */
    protected $pkgVersion = '1.0.5';

    /**
     * Override for package description.
     */
    public function getPackageDescription()
    {
        return t('Debug package for clockwork chrome extension.');
    }

    /**
     * Override for package name.
     */
    public function getPackageName()
    {
        return t('Clockwork Debug');
    }

    /**
     * Package entry point.
     */
    public function on_start()
    {
        $pkg = Package::getByHandle('debug');

        Start::start($pkg);
    }

    /**
     * Override for package install.
     */
    public function install()
    {
        $pkg = parent::install();

        Install::install($pkg);
    }

    /**
     * Override for package upgrade.
     */
    public function upgrade()
    {
        parent::upgrade();

        Install::install($this);
    }
}
