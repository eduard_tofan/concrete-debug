<?php

namespace Concrete\Package\Debug\Controller\SinglePage\Dashboard;

use Config;
use Symfony\Component\Console\Input\ArrayInput;
use Symfony\Component\Console\Output\NullOutput;
use Concrete\Core\Page\Controller\DashboardPageController;
use Concrete\Core\Console\Command\GenerateIDESymbolsCommand;

class Debug extends DashboardPageController
{
    /**
     * View method.
     */
    public function view()
    {
        $this->set('clockwork', Config::get('debug::debug.enable_clockwork'));
        $this->set('fire', Config::get('debug::debug.enable_fire'));
        $this->set('doctrine', Config::get('debug::debug.doctrine_data_sourced_enabled'));
    }

    /**
     * Save method.
     */
    public function save()
    {
        if ($this->request('ide_symbols')) {
            $this->ideSymbols();
        }

        if ($this->request('clockwork') == 'none') {
            // disable sql logging
            Config::save('debug::debug.enable_clockwork', false);
            Config::save('debug::debug.enable_fire', false);
        } else {
            $clockwork = false;

            if ($this->request('clockwork') == 'clockwork') {
                $clockwork = true;
            }

            Config::save('debug::debug.enable_clockwork', $clockwork);
            Config::save('debug::debug.enable_fire', ! $clockwork);
        }

        Config::save('debug::debug.doctrine_data_sourced_enabled', $this->request('doctrine') ? true : false);

        $this->redirect('/dashboard/debug/saved');
    }

    /**
     * Generate IDE symbols.
     *
     * @throws \Exception
     */
    protected function ideSymbols()
    {
        $command = new GenerateIDESymbolsCommand();

        $command->run(new ArrayInput([]), new NullOutput());

        $this->redirect('/dashboard/debug/generated');
    }

    /**
     * Display save success message.
     */
    public function saved()
    {
        $this->set('message', tc('debug', 'Settings saved.'));

        $this->view();
    }

    /**
     * Display generation success message.
     */
    public function generated()
    {
        $this->set('message', tc('debug', 'Symbols file generated.'));

        $this->view();
    }
}
