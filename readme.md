# Clockwork Debug

Debug package.

## Installation

```
- copy the package into the packages folder
- composer install
- install the package from the dashboard
```

## Configuration

> Only one option can be active at a time.  
> IDE symbols can be generated from the __/dashboard/debug__ page.  
> The clockwork extension provides data about the request.
    
## Eloquent configuration
For eloquent queries to be logged you must add the following line of code after eloquent has booted:

Keep in mind that this event must be fired after the debug package has been initiated. So the package which emits
the event must be installed after the debug package.
```
\Events::fire('eloquent_booted', $capsule);
```
Where `$capsule` is your `Illuminate\Database\Capsule\Manager` instance.

![Config](./doc/Screenshot_2.png "Clockwork")

### Clockwork Chrome extension. [[Download]](https://chrome.google.com/webstore/detail/clockwork/dmggabnehkmmfmdffgajcflpdjlnoemp?hl=en)

![Chrome](./doc/Screenshot_1.png "Clockwork")

### Firebug + FirePHP Firefox extension. [[Download]](https://addons.mozilla.org/en-US/firefox/addon/firebug/)

![Firefox](./doc/Screenshot_3.png "Firebug")
