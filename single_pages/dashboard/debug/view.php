<form method="post" action="<?= $view->action('save') ?>">
    <div class="form-group">
        <label><?= tc('debug', 'Database logging') ?></label>
        <div class="radio">
            <label>
                <input type="radio" name="clockwork" value="clockwork" <?= $clockwork ? 'checked' : '' ?>>
                Chrome Clockwork
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="clockwork" value="fire" <?= $fire ? 'checked' : '' ?>>
                Firefox FirePHP
            </label>
        </div>
        <div class="radio">
            <label>
                <input type="radio" name="clockwork" value="none" <?= ! $clockwork && ! $fire ? 'checked' : '' ?>>
                None
            </label>
        </div>
    </div>

    <div class="form-group">
        <div class="checkbox">
            <label>
                <input type="checkbox" name="doctrine" value="1" <?= $doctrine ? 'checked' : '' ?>>
                Enable concrete doctrine database logging
            </label>
        </div>
    </div>

    <hr>

    <div class="form-group">
        <button class="btn btn-default" name="ide_symbols" value="1"><?= tc('debug', 'Generate IDE symbols') ?></button>
    </div>
    <div class="ccm-dashboard-form-actions-wrapper">
        <div class="ccm-dashboard-form-actions">
            <button class="pull-right btn btn-primary" type="submit" ><?= tc('debug', 'Save') ?></button>
        </div>
    </div>
</form>
