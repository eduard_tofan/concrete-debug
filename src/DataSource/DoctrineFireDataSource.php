<?php

namespace Concrete\Package\Debug\Src\DataSource;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Logging\SQLLogger;
use Concrete\Package\Debug\Src\Debug\Fire;

class DoctrineFireDataSource implements SQLLogger
{
    /**
     * Internal array where queries are stored.
     *
     * @var array
     */
    protected $queries = array();

    /**
     * For timing queries:
     *
     * @var string
     */
    public $start = null;

    /**
     * Current recorded query
     *
     * @var string
     */
    public $query = null;

    /**
     * Doctrine connection
     *
     * @var Connection
     */
    protected $connection;

    /**
     * Doctrine connection
     *
     * @var Fire
     */
    protected $fire;

    /**
     * DoctrineDataSource constructor.
     *
     * @param $connection
     * @param $fire
     * @param bool $doctrineEnabled
     */
    public function __construct($connection, $fire, $doctrineEnabled = true)
    {
        $this->connection = $connection;

        if ($doctrineEnabled) {
            $this->connection->getConfiguration()->setSQLLogger($this);
        }

        $this->fire = $fire;

        $this->queries[] = ['Query', 'Duration', 'Connection'];

        if (class_exists('\ZendDb')) {
            $this->registerZendDataSource();
        }
    }

    /**
     * From SQLLogger Doctrine Interface
     *
     * @param string $sql
     * @param array $params
     * @param array $types
     */
    public function startQuery($sql, array $params = null, array $types = null)
    {
        $this->start = microtime(true);
        $sql         = $this->replaceParams($sql, $params);
        $sql         = $this->formatQuery($sql);

        $this->query = array('sql' => $sql, 'params' => $params, 'types' => $types);
    }

    /**
     * Formats query.
     *
     * @param $sql
     * @return mixed
     */
    protected function formatQuery($sql)
    {
        $keywords = array('select', 'insert', 'update', 'delete', 'where', 'from', 'limit', 'is', 'null', 'having', 'group by', 'order by', 'asc', 'desc');
        $regexp   = '/\b' . implode('\b|\b', $keywords) . '\b/i';

        $sql = preg_replace_callback($regexp, function($match){
            return strtoupper($match[0]);
        }, $sql);

        return $sql;
    }

    /**
     * Replaces params.
     *
     * @param $sql
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    protected function replaceParams($sql, $params)
    {
        if (is_array($params)) {
            $pattern = '/\?/';

            foreach ($params as $param) {
                $param = $this->convertParam($param);
                $sql   = preg_replace($pattern, "\"$param\"", $sql, 1);
            }
        }

        return $sql;
    }

    /**
     * Converts params.
     *
     * @param $param
     * @return mixed
     * @throws \Exception
     */
    protected function convertParam($param)
    {
        if (is_object($param)) {
            if (! method_exists($param, '__toString')) {
                // Carbon Object
                if (is_a($param, 'DateTime') || is_a($param, 'DateTimeImmutable')) {
                    $param = $param->format('Y-m-d');
                } else {

                    throw new \Exception('Unstringable Object: '.get_class($param));
                }
            }
        }

        return $param;
    }

    /**
     * From SQLLogger Doctrine Interface
     */
    public function stopQuery()
    {
        $endTime = microtime(true) - $this->start;

        $this->registerQuery($this->query['sql'], $endTime, 'DOCTRINE');
    }

    /**
     * Log the query into the internal store
     *
     * @param string $query
     * @param float $time
     * @param $connection
     */
    public function registerQuery($query, $time, $connection)
    {
        $this->queries[] = [$query, round($time * 1000, 3) . ' ms', $connection];

        if (! headers_sent()) {

            $this->fire->table("SQL Queries", $this->queries);
        }
    }

    /**
     * Registers the zend db.
     */
    protected function registerZendDataSource()
    {
        $source = new ZendDataSource(\ZendDb::adapter());

        \Events::addListener('zendFinishedQuery', function ($event) use ($source) {

            $query = $event->getArguments()['arguments'][0];

            $params = $query['parameters'] ? $query['parameters']->getNamedArray() : [];

            $sql = $source->formatQuery($source->replaceParams($query['sql'], $params));

            $time = (string) ($query['end'] - $query['start']);

            $this->registerQuery($sql , $time, 'ZEND');
        });
    }

    /**
     * Registers the eloquent db.
     *
     * @param \Illuminate\Database\Capsule\Manager $manager
     */
    public function registerEloquentDataSource($manager)
    {
        $source = new EloquentDataSource($manager, $manager->getEventDispatcher());

        \Events::addListener('eloquentFinishedQuery', function ($args) use ($source) {
            $event = $args->getArguments()['arguments'][0];
            $this->registerQuery(
                $source->createRunnableQuery($event->sql, $event->bindings, $event->connectionName),
                $event->time / 1000,
                'ELOQUENT'
            );
        });
    }
}
