<?php

namespace Concrete\Package\Debug\Src\DataSource;

class PhpDataSource extends \Clockwork\DataSource\PhpDataSource
{
    /**
     * Return POST data (replace unserializable items, attempt to remove passwords)
     */
    protected function getPostData()
    {
        $input = json_decode(file_get_contents('php://input'), true) ?: [];

        return $this->removePasswords(
            $this->replaceUnserializable(array_merge($_POST, $input))
        );
    }
}
