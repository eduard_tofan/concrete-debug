<?php

namespace Concrete\Package\Debug\Src\DataSource;

use Clockwork\Request\Request;
use Zend\Db\Adapter\Profiler\Profiler;
use Clockwork\DataSource\DataSourceInterface;

class ZendDataSource extends Profiler implements DataSourceInterface
{
    /**
     * Internal array where queries are stored
     *
     * @var array
     */
    protected $queries = array();

    /**
     * Doctrine connection
     *
     * @var \Concrete\Package\RevenewToolkit\Src\Db\Adapter\Adapter
     */
    protected $connection;

    /**
     * DoctrineDataSource constructor.
     *
     * @param \Concrete\Package\RevenewToolkit\Src\Db\Adapter\Adapter $connection
     */
    public function __construct($connection)
    {
        $this->connection = $connection;

        $this->connection->setProfiler($this);
    }

    /**
     * Formats the query.
     *
     * @param $sql
     * @return mixed
     */
    public function formatQuery($sql)
    {
        $keywords = array('select', 'insert', 'update', 'delete', 'where', 'from', 'limit', 'is', 'null', 'having', 'group by', 'order by', 'asc', 'desc');
        $regexp   = '/\b' . implode('\b|\b', $keywords) . '\b/i';

        $sql = preg_replace_callback($regexp, function($match){
            return strtoupper($match[0]);
        }, $sql);

        return $sql;
    }

    /**
     * Replaces the query params.
     *
     * @param $sql
     * @param $params
     * @return mixed
     * @throws \Exception
     */
    public function replaceParams($sql, $params)
    {
        if (is_array($params)) {

            foreach ($params as $key => $value)
            {
                $sql = str_replace(':' . $key, $this->convertParam($value), $sql);
            }
        }

        return $sql;
    }

    /**
     * Converts the query params.
     *
     * @param $param
     * @return mixed
     * @throws \Exception
     */
    public function convertParam($param)
    {
        if (is_object($param)) {
            if (! method_exists($param, '__toString')) {
                // Carbon Object
                if (is_a($param, 'DateTime') || is_a($param, 'DateTimeImmutable')) {
                    $param = $param->format('Y-m-d');
                } else {

                    throw new \Exception('Unstringable Object: '.get_class($param));
                }
            }
        } else if (is_string($param)) {

            $param = "'" . $param . "'";
        }

        return $param;
    }

    /**
     * Log the query into the internal store
     *
     * @param string $query
     * @param array $bindings
     * @param float $time
     * @param string $connection
     * @return array
     */
    public function registerQuery($query, $bindings, $time, $connection)
    {
        $this->queries[] = [
            'query'      => $query,
            'bindings'   => $bindings,
            'time'       => $time * 1000,
            'connection' => $connection
        ];
    }

    /**
     * Adds ran database queries to the request
     *
     * @param Request $request
     * @return Request
     */
    public function resolve(Request $request)
    {
        $request->databaseQueries = array_merge($request->databaseQueries, $this->getDatabaseQueries());

        return $request;
    }

    /**
     * Returns an array of runnable queries and their durations from the internal array
     *
     * @return array
     */
    public function getDatabaseQueries()
    {
        $this->getZendQueries();

        $queries = array();

        foreach ($this->queries as $query) {
            $queries[] = array(
                'query'      => $query['query'],
                'duration'   => $query['time'],
                'connection' => $query['connection']
            );
        }

        return $queries;
    }

    /**
     * Returns an array of runnable queries and their durations from the internal array
     *
     * @return array
     */
    public function getQueries()
    {
        $this->getZendQueries();

        $queries = array();

        foreach ($this->queries as $query) {

            $queries[] = [$query['query'], (string) $query['time'], $query['connection']];
        }

        return $queries;
    }

    /**
     * Returns an array of runnable queries and their durations from the internal array
     */
    protected function getZendQueries()
    {
        foreach ($this->getProfiles() as $query) {

            $sql = $this->formatQuery($this->replaceParams($query['sql'], $query['parameters']->getNamedArray()));

            $time = $query['end'] - $query['start'];

            $this->registerQuery($sql, $query['parameters']->getNamedArray(), $time, 'ZEND');
        }
    }

    public function profilerFinish()
    {
        $result = parent::profilerFinish();

        \Events::fire('zendFinishedQuery', $this->profiles[$this->currentIndex - 1]);

        return $result;
    }
}
