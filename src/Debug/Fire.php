<?php

namespace Concrete\Package\Debug\Src\Debug;

use Concrete\Package\Debug\Src\DataSource\DoctrineFireDataSource;

class Fire extends \FirePHP
{
    /**
     * Doctrine data-source.
     *
     * @var array
     */
    protected $dataSource;

    /**
     * Fire constructor.
     *
     * @param bool $doctrineEnabled
     */
    public function __construct($doctrineEnabled = true)
    {
        $this->dataSource = new DoctrineFireDataSource(\Database::connection(), $this, $doctrineEnabled);

        $this->setFireHeaders();
    }

    /**
     * Get the data source
     *
     * @return array|DoctrineFireDataSource
     */
    public function getDataSource()
    {
        return $this->dataSource;
    }

    /**
     * Logs a table.
     *
     * @param string $label
     * @param string $object
     * @return true|void
     */
    public function table($label, $object)
    {
        $msgMeta = [];

        $msgMeta['Type'] = 'TABLE';

        if ($label !== null) {

            $msgMeta['Label'] = $label;
        }

        if (isset($meta['file']) && !isset($msgMeta['File'])) {

            $msgMeta['File'] = $meta['file'];
        }

        if (isset($meta['line']) && !isset($msgMeta['Line'])) {

            $msgMeta['Line'] = $meta['line'];
        }

        $msg = '[' . $this->jsonEncode($msgMeta) . ',' . $this->jsonEncode($object, true) . ']';

        $parts = explode("\n", chunk_split($msg, 5000, "\n"));

        for ($i = 0; $i < count($parts); $i++) {

            $part = $parts[$i];

            if ($part) {

                if (count($parts) > 2) {
                    // Message needs to be split into multiple parts
                    $this->setHeader('X-Wf-1-1-' . '1-' . ($i + 1), (($i == 0) ? strlen($msg) : '') . '|' . $part . '|' . (($i < count($parts) - 2) ? '\\' : ''));

                } else {

                    $this->setHeader('X-Wf-1-1-' . '1-' . ($i + 1), strlen($part) . '|' . $part . '|');
                }
            }
        }
    }

    /**
     * Send header
     *
     * @param string $name
     * @param string $value
     */
    public function setHeader($name, $value)
    {
        return header($name. ': ' . $value);
    }

    /**
     * Encode table.
     *
     * @param array|object $object
     * @param bool $skipObjectEncode
     * @return mixed|string
     */
    public function jsonEncode($object, $skipObjectEncode = false)
    {
        if(! $skipObjectEncode) {
            $object = $this->encodeObject($object);
        }

        if(function_exists('json_encode') && $this->options['useNativeJsonEncode'] != false) {

            return json_encode($object);

        } else {

            return $this->jsonEncode($object);
        }
    }

    /**
     * Set the necessary headers for the firePHP extension.
     */
    private function setFireHeaders()
    {
        $this->setHeader('X-Wf-Protocol-1','http://meta.wildfirehq.org/Protocol/JsonStream/0.2');

        $this->setHeader('X-Wf-1-Plugin-1','http://meta.firephp.org/Wildfire/Plugin/FirePHP/Library-FirePHPCore/' . FirePHP_VERSION);

        $this->setHeader('X-Wf-1-Structure-1','http://meta.firephp.org/Wildfire/Structure/FirePHP/FirebugConsole/0.1');
    }
}
