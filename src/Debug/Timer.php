<?php

namespace Concrete\Package\Debug\Src\Debug;

class Timer
{
    /*
     * @var int
     */
    protected $start;

    /*
     * @var int
     */
    protected $end;

    /**
     * Timer constructor.
     */
    public function __construct()
    {
        $this->start = null;
        $this->end = null;
    }

    /**
     * Start the timer.
     *
     * @return $this
     */
    public function start()
    {
        $this->start = microtime(true);
        $this->end = null;

        return $this;
    }

    /**
     * Stop the timer
     *
     * @param bool $seconds
     * @param bool $log
     * @return float|string
     */
    public function stop($seconds = false, $log = true)
    {
        $this->end = microtime(true);

        $result = $this->end - $this->start;
        $result = $seconds ? "$result s" : $result * 1000 . ' ms';

        if ($log) {
            clockwork()->info($result);
        }

        return $result;
    }

    /**
     * Reset the timer.
     *
     * @return $this
     */
    public function reset()
    {
        $this->start = null;
        $this->end = null;

        return $this;
    }
}
