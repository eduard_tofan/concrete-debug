<?php

if (! function_exists('clockwork'))
{
    /**
     * Get the clockwork instance for debugging.
     *
     * @return \Clockwork\Clockwork
     */
    function clockwork()
    {
        return \Core::make('clockwork');
    }
}

if (! function_exists('timer_start'))
{
    /**
     * Start a timer.
     *
     * @return \Concrete\Package\Debug\Src\Debug\Timer
     */
    function timer_start()
    {
        return \Core::make('timer')->start();
    }
}

if (! function_exists('timer_stop'))
{
    /**
     * End a timer and reset it.
     *
     * @param bool $seconds
     * @return \Concrete\Package\Debug\Src\Debug\Timer
     */
    function timer_stop($seconds = false)
    {
        $result = \Core::make('timer')->stop($seconds);
        \Core::make('timer')->reset();

        return $result;
    }
}

if (! function_exists('fire'))
{
    /**
     * Get the fire instance for debugging.
     *
     * @return \Concrete\Package\Debug\Src\Debug\Fire
     */
    function fire()
    {
        return \Core::make('fire');
    }
}
