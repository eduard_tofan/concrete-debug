<?php

namespace Concrete\Package\Debug\Src\Package;

use Job;
use Log;
use Page;
use Group;
use Config;
use JobSet;
use PageType;
use BlockType;
use Exception;
use SinglePage;
use PageTemplate;
use CollectionAttributeKey;
use Concrete\Core\Package\Package;
use Concrete\Core\Page\Theme\Theme;
use Concrete\Core\Attribute\Set as AttributeSet;
use Concrete\Core\Attribute\Type as AttributeType;
use Concrete\Core\Attribute\Key\Category as AttributeKeyCategory;
use Concrete\Core\Page\Type\Composer\Control\Type\Type as PageTypeComposerControlType;
use Concrete\Core\Page\Type\PublishTarget\Type\Type as PageTypePublishTargetType;

defined('C5_EXECUTE') or die(_('Access Denied.'));

class Install
{

    /**
     * @not yet deprecated Use UpgradeManager or extend RevenewToolkit\Src\Package\Package instead.
     *
     * Installs additional package resources from the package install.php config file. This method should be called
     * after the package has already been installed; the package controller install method should thus look like this:
     *
     *  public function install() {
     *      $package = parent::install();
     *      Src\Support\Package\Install::install($package);
     *      ...
     *
     *
     * @param Package $package
     * @throws Exception
     */
    public static function install(Package $package)
    {
        $packageHandle = $package->getPackageHandle();
        $package = Package::getByHandle($packageHandle);

        // Verify if the package has been installed.
        if (!$package instanceof Package)
            throw new Exception('Invalid package.');

        $package->registerConfigNamespace();

        // core attribute sets
        static::attributeSets(
            Config::get(sprintf('%s::install.attribute_sets', $packageHandle), array()),
            $package
        );

        // core attribute types
        static::attributeTypes(
            Config::get(sprintf('%s::install.attribute_types', $packageHandle), array()),
            $package
        );

        // core attributes
        static::attributes(
            Config::get(sprintf('%s::install.attributes', $packageHandle), array()),
            $package
        );

        static::config(
            Config::get(sprintf('%s::install.config', $packageHandle), [])
        );

        // core groups
        static::groups(
            Config::get(sprintf('%s::install.groups', $packageHandle), array()),
            $package
        );

        // core jobs
        static::jobs(
            Config::get(sprintf('%s::install.jobs', $packageHandle), array()),
            $package
        );

        // core single pages
        static::singlePages(
            Config::get(sprintf('%s::install.singlepages', $packageHandle), array()),
            $package
        );

        // core templates
        static::templates(
            Config::get(sprintf('%s::install.templates', $packageHandle), array()),
            $package
        );

        // core page types
        static::pageTypes(
            Config::get(sprintf('%s::install.pagetypes', $packageHandle), array()),
            $package
        );

        // core themes
        static::themes(
            Config::get(sprintf('%s::install.themes', $packageHandle), array()),
            $package
        );

        // core blocks
        static::blocks(
            Config::get(sprintf('%s::install.blocks', $packageHandle), array()),
            $package
        );
    }

    /**
     * Installs configuration entries if not done before.
     *
     * @param Package $package
     */
    public static function installConfig(Package $package)
    {
        $packageHandle = $package->getPackageHandle();

        $configKey = 'concrete.revenew_toolkit.initial_config_deployed';

        if (!Config::get($configKey)) {
            // Configuration has not been installed yet.

            static::config(
                Config::get(sprintf('%s::install.config', $packageHandle), [])
            );

            // Mark configuration installation done.
            Config::save($configKey, true);
        }
    }

    /**
     * @not yet deprecated Use UpgradeManager or extend RevenewToolkit\Src\Package\Package instead.
     *
     * Upgrades the package, its database and resources to the latest version. This method takes care of the entire
     * upgrade process so it can replace any standard code in the package controller upgrade method,
     *
     * For example:
     *
     *  public function upgrade() {
     *      Src\Support\Package\Install::upgrade($this);
     *  }
     *
     * @param Package $package
     * @throws Exception
     */
    public static function upgrade(Package $package)
    {
        $package = Package::getByHandle($package->getPackageHandle());

        // Verify if the package has been installed.
        if (!$package instanceof Package)
            throw new Exception('Invalid package.');

        $package->upgradeDatabase();

        // Refresh all blocks
        $items = $package->getPackageItems();
        if (is_array($items['block_types'])) {
            foreach ($items['block_types'] as $item) {
                /* @var BlockType $item */
                $item->refresh();
            }
        }
        \Localization::clearCache();

        // Re-run additional package resource installation to make sure everything is up-to-date.
        static::install($package);
    }

    /**
     * Installs default configuration entries.
     *
     *      'config' => [
     *          ['config.path', 'value'], // This config entry will be created in the root config namespace.
     *          ['config.path2', 'foo', 'revenew_toolkit'], This config entry will be created in the revenew_toolkit (package) namespace.
     *          ...
     *      ],
     *
     * @param array $data
     */
    public function config(array $data)
    {

        foreach ($data as $entry) {
            if (is_array($entry)) {

                try {

                    $key = array_shift($entry);
                    $value = array_shift($entry);
                    $namespace = array_shift($entry);

                    if ($namespace)
                        $key = sprintf('%s::%s', $namespace, $key);

                    if (!Config::has($key)) {
                        Config::save($key, $value);
                    }

                } catch (Exception $e) {
                    Log::error($e);
                }
            }
        }

    }

    /**
     * Installs package groups defined in the config.
     *
     *      'groups' => [
     *          [
     *              'name' => 'My First Group',
     *              'description' => 'Look at my shiny first group',
     *              'path' => '/' // (or /Administrators for example, used to create a parent/child group structure
     *          ],
     *          ...
     *
     * @param array $groups
     * @param Package $package
     */
    public static function groups($groups, Package $package)
    {
        foreach ($groups as $group) {
            try {
                isset($group['description']) or $group['description'] = '';
                isset($group['path']) or $group['path'] = '/';

                // Check if the group already exists so we won't get unnecessary clutter
                $groupPath = rtrim($group['path'], '/') . '/' . $group['name'];
                if (Group::getByPath($groupPath)) {
                    throw new \Exception(t('Group "%s" already exists.', $groupPath));
                }

                // Get the parent group (if any)
                $parentGroup = false;
                if (($groupPath = trim($group['path'])) && ($groupPath != '/')) {
                    $parentGroup = Group::getByPath($groupPath);
                    if (!$parentGroup) {
                        throw new \Exception(t('No group found at "%s"', $groupPath));
                    }
                }

                // Add the group
                Group::add(
                    $group['name'],
                    $group['description'],
                    $parentGroup ? $parentGroup : false,
                    $package
                );
            } catch (\Exception $e) {
                Log::error($e);
            }
        }
    }

    /**
     * Installs package jobs defined in the config.
     *
     *      'jobs' => [
     *          [
     *              'handle' => 'my_first_concrete_job',
     *              'sets' => [
     *                  'Default',
     *                  ...
     *              ],
     *          ],
     *          ...
     *
     * @param array $jobs
     * @param Package $package
     */
    public static function jobs(array $jobs, Package $package)
    {
        /* @var JobSet[] $jobSets */
        $jobSets = array();
        foreach ($jobs as $jobData) {
            try {
                Job::getByHandle($jobData['handle']) or Job::installByPackage($jobData['handle'], $package); // Check if it exists and don't spam event log
                $job = Job::getByHandle($jobData['handle']); // reload otherwise jID is not set
                if (isset($jobData['sets']) && is_array($jobData['sets'])) {
                    foreach ($jobData['sets'] as $jobSetName) {
                        if (!isset($jobSets[$jobSetName])) {
                            $jobSets[$jobSetName] = JobSet::getByName($jobSetName);
                        }
                        if (!isset($jobSets[$jobSetName])) {
                            $jobSets[$jobSetName] = JobSet::add($jobSetName, $package);
                        }
                        if (isset($jobSets[$jobSetName])) {
                            $jobSets[$jobSetName]->addJob($job);
                        }
                    }
                }
            } catch (\Exception $e) {
                Log::error($e);
            }
        }
    }

    /**
     * Installs package single pages defined in the config.
     *
     *      'pages' => [
     *          [
     *              'path' => '/path/to/singlepage',
     *              'name' => 'Singlepage',
     *              'description' => 'Description of the singlepage',
     *              'data' => [
     *                  'cIsSystemPage' => 0,
     *                  ...
     *              ],
     *              'attributes' => [
     *                  ...
     *              ],
     *          ],
     * @param array $singlePages
     * @param Package $package
     */
    public static function singlePages(array $singlePages, Package $package)
    {
        foreach ($singlePages as $pageInfo) {
            $page = null;

            /* @var Page $page */
            if (!($page = SinglePage::add($pageInfo['path'], $package))) {
                $page = Page::getByPath($pageInfo['path']);
            }

            if ($page) {
                $page->update(array(
                    'cName' => $pageInfo['name'],
                    'cDescription' => $pageInfo['description'],
                ));

                if (isset($pageInfo['attributes'])) {
                    foreach ($pageInfo['attributes'] as $key => $value) {
                        $page->setAttribute($key, $value);
                    }
                }
            }
        }
    }

    /**
     * Installs package themes defined in the config
     *
     *       'themes' => array(
     *            array(
     *               'handle' => 'theme_handle'
     *            ),
     *        ),
     *
     * @param mixed $themes
     * @param Package $package
     */
    public static function themes(array $themes, Package $package)
    {
        if (is_array($themes)) {
            foreach ($themes as $item) {
                $thHandle = $item['handle'];
                $th = Theme::getByHandle($thHandle);
                if (empty($th)) {
                    Theme::add($thHandle, $package);
                }
            }
        }
    }

    /**
     * add page template
     * @param array $templates
     * @param Package $pkg
     */
    public static function templates(array $templates, Package $pkg)
    {
        if (is_array($templates)) {
            foreach ($templates as $item) {
                $handle = $item['handle'];
                $pt = PageTemplate::getByHandle($handle);
                if (empty($pt)) {
                    $th = PageTemplate::add($handle, $item['name'], FILENAME_PAGE_TEMPLATE_DEFAULT_ICON, $pkg);
                }
            }
        }
    }

    /**
     * Installs package page types defined in the config
     *
     * 'pagetypes' => array(
     * array(
     * 'handle' => 'revenew_report',
     * 'name' => 'Revenew report',
     * 'ptLaunchInComposer' => true,
     * 'ptIsFrequentlyAdded' => true,
     * 'allowedTemplates' => 'A',
     * 'defaultTemplateHandle' => 'full',
     * 'attributeForms' => array(
     * array(
     * 'formName' => t('Report'),
     * 'formDescription' => t('Revenew report'),
     * 'attributes' => array(
     * 'revenew_report_dealergroup',
     * 'revenew_report_email_send',
     * 'revenew_report_date_from',
     * 'revenew_report_date_to',
     * 'revenew_report_email_frequency',
     * 'revenew_report_email_subject',
     * 'revenew_report_email_content',
     * ),
     * ),
     * ),
     * ),
     * ),
     *
     * @param mixed $pageTypes
     * @param Package $package
     */
    public static function pageTypes($pageTypes, Package $package)
    {
        if (is_array($pageTypes)) {
            foreach ($pageTypes as $item) {
                $pt = PageType::getByHandle($item['handle']);

                if (!$pt) {
                    $pTplHandle = $item['defaultTemplateHandle'] ?: 'full';
                    $pTpl = PageTemplate::getByHandle($pTplHandle);

                    if ($pTpl) {
                        $pt = PageType::add(array(
                            'handle' => $item['handle'],
                            'name' => $item['name'],
                            'ptLaunchInComposer' => $item['ptLaunchInComposer'] ? true : false,
                            'ptIsFrequentlyAdded' => $item['ptIsFrequentlyAdded'] ? true : false,
                            'allowedTemplates' => $item['allowedTemplates'] ?: 'A',
                            'defaultTemplate' => $pTpl,
                            'templates' => array()
                        ), $package);
                    }

                    if ($pt) {
                        // add page type form elements
                        $pts = $pt->addPageTypeComposerFormLayoutSet(t('Basics'), t('Basic data'));
                        if ($pts) {
                            $type = PageTypeComposerControlType::getByHandle('core_page_property');

                            $control = $type->getPageTypeComposerControlByIdentifier('name');
                            if ($control) {
                                $control->addToPageTypeComposerFormLayoutSet($pts);
                            }

                            $control = $type->getPageTypeComposerControlByIdentifier('description');
                            if ($control) {
                                $control->addToPageTypeComposerFormLayoutSet($pts);
                            }

                            $control = $type->getPageTypeComposerControlByIdentifier('url_slug');
                            if ($control) {
                                $control->addToPageTypeComposerFormLayoutSet($pts);
                            }

                            $control = $type->getPageTypeComposerControlByIdentifier('page_template');
                            if ($control) {
                                $control->addToPageTypeComposerFormLayoutSet($pts);
                            }

                            $control = $type->getPageTypeComposerControlByIdentifier('publish_target');
                            if ($control) {
                                $control->addToPageTypeComposerFormLayoutSet($pts);
                            }
                        }

                        // add target or it will break when you create a new page
                        $target = PageTypePublishTargetType::getByHandle('all');
                        if ($target) {
                            $configuredTarget = $target->configurePageTypePublishTarget($pt, null);
                            if ($configuredTarget) {
                                $pt->setConfiguredPageTypePublishTargetObject($configuredTarget);
                            }
                        }

                        // add custom attributes to composer
                        $type = PageTypeComposerControlType::getByHandle('collection_attribute');
                        if (is_array($item['attributeForms'])) {
                            foreach ($item['attributeForms'] as $form) {
                                // create form layout
                                $pts = $pt->addPageTypeComposerFormLayoutSet($form['formName'], $form['formDescription']);

                                // add attributes
                                foreach ($form['attributes'] as $attrHandle) {
                                    $attrKey = CollectionAttributeKey::getByHandle($attrHandle);
                                    $control = $type->getPageTypeComposerControlByIdentifier($attrKey->getAttributeKeyID());
                                    if ($control) {
                                        $control->addToPageTypeComposerFormLayoutSet($pts);
                                    }
                                }

                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Installs package blocks defined in the config
     *
     *   'blocks' => array(
     *        array(
     *            'handle' => 'block_handle',
     *        ),
     *    ),
     *
     * @param mixed $blocks
     * @param Package $package
     */
    public static function blocks($blocks, Package $package)
    {
        if (is_array($blocks)) {
            foreach ($blocks as $item) {
                $btHandle = $item['handle'];
                $bt = BlockType::getByHandle($btHandle);
                if (!$bt) {
                    BlockType::installBlockType($btHandle, $package);
                }
            }
        }
    }

    /**
     * Installs package attribute sets defined in the config
     *
     *       'attribute_sets' => array(
     *            array(
     *               'name' => 'My attribute set',
     *               'handle' => 'my_attribute_set',
     *               'entity' => 'collection', // collection|user|file
     *            ),
     *        ),
     *
     * @param mixed $attributeSets
     * @param Package $package
     */
    public function attributeSets(array $attributeSets, Package $package)
    {
        if (is_array($attributeSets)) {
            foreach ($attributeSets as $item) {
                $akCatSet = AttributeSet::getByHandle($item['handle']);
                if (!$akCatSet) {
                    $akCat = AttributeKeyCategory::getByHandle($item['entity']);
                    if ($akCat) {
                        $akCatSet = $akCat->addSet($item['handle'], $item['name'], $package);
                    }
                }
            }
        }
    }

    /**
     * Installs package attribute types defined in the config
     *
     *       'attribute_types' => array(
     *            array(
     *               'name' => 'My attribute type',
     *               'handle' => 'my_attribute_type',
     *               'entity' => 'collection', // collection|user|file
     *            ),
     *        ),
     *
     * @param mixed $attributeTypes
     * @param Package $package
     */
    public function attributeTypes($attributeTypes, Package $package)
    {
        if (is_array($attributeTypes)) {
            foreach ($attributeTypes as $item) {
                $akType = AttributeType::getByHandle($item['handle']);
                if (!$akType) {
                    $akType = AttributeType::add($item['handle'], $item['name'], $package);
                    if ($akType && $item['entity']) {
                        foreach ($item['entity'] as $v) {
                            $akCat = AttributeKeyCategory::getByHandle($v);
                            if ($akCat) {
                                $akCat->associateAttributeKeyType($akType);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Installs package attributes defined in the config
     *
     *       'attributes' => array(
     *            array(
     *               // required
     *               'name' => 'My attribute',
     *               'type' => 'text', // text|textarea|number|select|image_file|boolean
     *               'entity' => 'collection', // collection|user|file
     *
     *               // optional
     *               All the other options can be fetched from the creation of an attribute form page.
     *               Go to /dashboard/users/attributes, add an attribute of your type,
     *               then in the creation form check for the html field names (akIsSearchableIndexed, akIsSearchable, etc).
     *            ),
     *        ),
     *
     * @param mixed $data
     * @param Package $package
     */
    public function attributes($data, Package $pkg)
    {
        foreach ($data as $item) {
            $attrType = AttributeType::getByHandle($item['type']);

            if ($attrType) {
                // get set
                if ($item['setHandle']) {
                    $akCatSet = AttributeSet::getByHandle($item['setHandle']);
                }

                // add attribute
                if (!$item['setHandle'] || $akCatSet) {
                    $attrKeyClass = ucwords($item['entity']) . 'AttributeKey';
                    $attrKey = $attrKeyClass::getByHandle($item['handle']);

                    if (!is_object($attrKey)) {
                        $item['akHandle'] = $item['handle'];
                        $item['akName'] = $item['name'];

                        unset($item['handle']);
                        unset($item['name']);

                        $attrKey = $attrKeyClass::add($attrType, $item, $pkg);

                        if ($akCatSet) {
                            $attrKey->setAttributeSet($akCatSet);
                        }
                    }
                }
            }

        }
    }
}
