<?php

namespace Concrete\Package\Debug\Src\Package;

use Core;
use Route;
use Config;
use Concrete\Core\Package\Package;
use Concrete\Core\Asset\AssetList;
use Concrete\Core\Foundation\ClassAliasList;
use Concrete\Core\Foundation\Service\ProviderList;

defined('C5_EXECUTE') or die(_('Access Denied.'));

class Start
{
    /**
     * Registers additional package resources at runtime.
     *
     * @param Package $package
     */
    public static function start(Package $package)
    {
        $packageHandle = $package->getPackageHandle();

        if (Config::get(sprintf('packages.%s.started', $packageHandle)) !== true)
            Config::set(sprintf('packages.%s.started', $packageHandle), true);
        else
            return;

        // Register package config namespace in case it isn't done already.
        $package->registerConfigNamespace();

        /* @var $app \Concrete\Core\Application\Application */
        $app = Core::getFacadeApplication();

        $assetList = AssetList::getInstance();
        $assetList->registerMultiple(Config::get(sprintf('%s::app.assets', $packageHandle), []));
        $assetList->registerGroupMultiple(Config::get(sprintf('%s::app.asset_groups', $packageHandle), []));

        $classAliasList = ClassAliasList::getInstance();
        $classAliasList->registerMultiple(Config::get(sprintf('%s::app.aliases', $packageHandle), []));
        $classAliasList->registerMultiple(Config::get(sprintf('%s::app.facades', $packageHandle), []));

        $providerList = new ProviderList($app);
        $providerList->registerProviders(Config::get(sprintf('%s::app.providers', $packageHandle), []));

        Route::registerMultiple(Config::get(sprintf('%s::app.routes', $packageHandle), []));
    }
}
