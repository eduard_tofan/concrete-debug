<?php

namespace Concrete\Package\Debug\Src\Provider;

use Clockwork\Clockwork;
use Concrete\Package\Debug\Src\DataSource\EloquentDataSource;
use Concrete\Package\Debug\Src\Debug\Timer;
use Concrete\Package\Debug\Src\Storage\FileStorage;
use Concrete\Package\Debug\Src\DataSource\PhpDataSource;
use Concrete\Package\Debug\Src\DataSource\ZendDataSource;
use Concrete\Core\Foundation\Service\Provider as ServiceProvider;
use Concrete\Package\Debug\Src\DataSource\DoctrineDataSource;
use Symfony\Component\EventDispatcher\GenericEvent;

defined('C5_EXECUTE') or die(_('Access Denied.'));

class ClockworkServiceProvider extends ServiceProvider
{

    /**
     * Register method for the service.
     */
    public function register()
    {
        $this->app->singleton('timer', function () {
            return new Timer();
        });

        // do not enable in not activated
        if (! \Config::get('debug::debug.enable_clockwork')) {

            return;
        }

        $this->registerSingleton();

        if (class_exists('\Illuminate\Database\Capsule\Manager') && class_exists('\Illuminate\Events\Dispatcher')) {
            $this->registerEloquentLogging();
        }

        // bind the dispatch event so we can send the extension
        // necessary header data
        \Events::addListener('on_before_dispatch', function () {

            $clockwork = \Core::make('clockwork');

            header('X-Clockwork-Id: ' . $clockwork->getRequest()->id, true);

            header('X-Clockwork-Version: ' . Clockwork::VERSION, true);

        });

        // bind the shutdown event, when all queries are done
        // so we can build our debug data
        \Events::addListener('on_shutdown', function () {

            $clockwork = \Core::make('clockwork');

            $clockwork->resolveRequest();

            $clockwork->storeRequest();

        });

        // register route necessary for the extension
        \Route::register('/__clockwork/{id}', function ($id) {

            $clockwork = \Core::make('clockwork');

            return $clockwork->getStorage()->retrieve($id, null)->toJson();
        });
    }

    /**
     * Registers the clockwork singleton.
     */
    public function registerSingleton()
    {
        // register the clockwork service provider
        // add doctrine query listener
        $this->app->singleton('clockwork', function () {

            $clockwork = new Clockwork();

            $clockwork->addDataSource(new PhpDataSource());

            $doctrineEnabled = \Config::get('debug::debug.doctrine_data_sourced_enabled', true);

            if ($doctrineEnabled) {
                $clockwork->addDataSource(new DoctrineDataSource(\Database::connection()));
            }

            if (class_exists('\ZendDb')) {
                $clockwork->addDataSource(new ZendDataSource(\ZendDb::adapter()));
            }

            $path = DIR_PACKAGES . '/' . 'debug/clockwork';

            $storage = new FileStorage($path);

            $clockwork->setStorage($storage);

            return $clockwork;
        });
    }

    /**
     * Registers the eloquent data source.
     */
    protected function registerEloquentLogging()
    {
        /**
         * @var GenericEvent $args
         */
        \Events::addListener('eloquent_booted', function ($args) {

            $clockwork = \Core::make('clockwork');

            if (! $clockwork) {
                $this->registerSingleton();
            }

            $clockwork = \Core::make('clockwork');

            /**
             * @var \Illuminate\Database\Capsule\Manager $manager
             */
            $manager = $args->getArguments()['arguments'][0];

            $clockwork->addDataSource(new EloquentDataSource($manager, $manager->getEventDispatcher()));
        });
    }
}
