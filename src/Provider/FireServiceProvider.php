<?php

namespace Concrete\Package\Debug\Src\Provider;

use Concrete\Package\Debug\Src\Debug\Fire;
use Concrete\Core\Foundation\Service\Provider as ServiceProvider;
use Symfony\Component\EventDispatcher\GenericEvent;

defined('C5_EXECUTE') or die(_('Access Denied.'));

class FireServiceProvider extends ServiceProvider
{
    /**
     * Register method for the service.
     */
    public function register()
    {
        // do not enable in not activated
        if (! \Config::get('debug::debug.enable_fire')) {

            return;
        }

        $this->registerSingleton();

        if (class_exists('\Illuminate\Database\Capsule\Manager') && class_exists('\Illuminate\Events\Dispatcher')) {
            $this->registerEloquentLogging();
        }

        // bind the dispatch event so we can send the extension
        // necessary header data
        \Events::addListener('on_before_dispatch', function () {

            \Core::make('fire');

        });
    }

    /**
     * Registers the clockwork singleton.
     */
    protected function registerSingleton()
    {
        // register the firePHP service provider
        // add doctrine query listener
        $this->app->singleton('fire', function () {

            $doctrineEnabled = \Config::get('debug::debug.doctrine_data_sourced_enabled', true);

            return new Fire($doctrineEnabled);

        });
    }

    /**
     * Registers the eloquent data source.
     */
    protected function registerEloquentLogging()
    {
        /**
         * @var GenericEvent $args
         */
        \Events::addListener('eloquent_booted', function ($args) {
            $fire = \Core::make('fire');

            if (! $fire) {
                $this->registerSingleton();
            }

            /**
             * @var Fire
             */
            $fire = \Core::make('fire');

            /**
             * @var \Illuminate\Database\Capsule\Manager $manager
             */
            $manager = $args->getArguments()['arguments'][0];

            $fire->getDataSource()->registerEloquentDataSource($manager);
        });
    }
}
