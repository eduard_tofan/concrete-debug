<?php

namespace Concrete\Package\Debug\Src\Storage;

use Clockwork\Request\Request;
use Clockwork\Storage\FileStorage as Storage;

class FileStorage extends Storage
{
    /**
     * Path where files are stored.
     *
     * @var string
     */
    protected $path;

    /**
     * Requests to keep.
     *
     * @var integer
     */
    protected $cachedRequestsCount;

    /**
     * FileStorage constructor.
     *
     * @param $path
     * @param int $dirPermissions
     * @param int $cachedRequestsCount
     * @throws \Exception
     */
    public function __construct($path, $dirPermissions = 0700, $cachedRequestsCount = 5)
    {
        $this->path = $path;

        $this->cachedRequestsCount = $cachedRequestsCount;

        parent::__construct($path, $dirPermissions);
    }

    /**
     * Store request,
     * requests are stored in JSON representation in files named <request id>.json in storage path
     *
     * @param Request $request
     */
    public function store(Request $request)
    {
        $files = @scandir($this->path);

        if (count($files) > $this->cachedRequestsCount + 2) {
            $files = array_slice($files, 2, count($files) - $this->cachedRequestsCount - 1);

            array_map(function ($el) { @unlink(rtrim($this->path, '/') . "/$el"); }, $files);
        }

        parent::store($request);
    }
}
